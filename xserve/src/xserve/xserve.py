from urllib.parse import urlparse
import xarray as xr
import logging
import sys
import pandas as pd
import datetime
import xpublish
import click
from dask.distributed import Client

logging.basicConfig(stream=sys.stderr,
                    level=logging.INFO,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%dT%H:%M:%S')

def get_vsi_url(item, folder, username=None, api_key=None):

    #endpoint = 'https://nx10438.your-storageshare.de/remote.php/webdav/ogc-tb16/s5p/tropospheric_NO2_column_number_density/'
    endpoint = 'https://store.terradue.com:443/api/ogc-tb16/s5p/tropospheric_NO2_column_number_density/'

    endpoint = 'https://s5-no2.s3.fr-par.scw.cloud'

    parsed_url = urlparse(endpoint)

    if username is not None:
        url = '/vsicurl/%s://%s:%s@%s%s%s' % (list(parsed_url)[0],
                                            username,
                                            api_key,
                                            list(parsed_url)[1],
                                            list(parsed_url)[2],
                                            item)

    else:

        url = f'/vsicurl/{list(parsed_url)[0]}://{list(parsed_url)[1]}{list(parsed_url)[2]}/{folder}/{item}'
        print(url)
    #url = '/data/{}/{}'.format(folder, item)

    return url

@click.command()
@click.option(
    "--folder",
    type=str,
    default=None
)
@click.option(
    "--port",
    type=int,
)
def main(folder, port):
    
    # tcp://scheduler:8786
    
    client = Client('tcp://scheduler:8786')
    logging.info(client)
    logging.info(client.ncores())

    s5p = ['s5p-l3-tropno2-month-201805-{}.tif',
           's5p-l3-tropno2-month-201806-{}.tif',
           's5p-l3-tropno2-month-201807-{}.tif',
           's5p-l3-tropno2-month-201808-{}.tif',
           's5p-l3-tropno2-month-201809-{}.tif',
           's5p-l3-tropno2-month-201810-{}.tif',
           's5p-l3-tropno2-month-201811-{}.tif',
           's5p-l3-tropno2-month-201812-{}.tif',
           's5p-l3-tropno2-month-201901-{}.tif',
           's5p-l3-tropno2-month-201902-{}.tif',
           's5p-l3-tropno2-month-201903-{}.tif',
           's5p-l3-tropno2-month-201904-{}.tif',
           's5p-l3-tropno2-month-201905-{}.tif',
           's5p-l3-tropno2-month-201906-{}.tif',
           's5p-l3-tropno2-month-201907-{}.tif',
           's5p-l3-tropno2-month-201908-{}.tif',
           's5p-l3-tropno2-month-201909-{}.tif',
           's5p-l3-tropno2-month-201910-{}.tif',
           's5p-l3-tropno2-month-201911-{}.tif',
           's5p-l3-tropno2-month-201912-{}.tif',
           's5p-l3-tropno2-month-202001-{}.tif',
           's5p-l3-tropno2-month-202002-{}.tif',
           's5p-l3-tropno2-month-202003-{}.tif',
           's5p-l3-tropno2-month-202004-{}.tif']



    dates = []
    datasets = []

    for item in s5p:

        logging.debug(item)

        # read the vsicurl geotiff
        da = xr.open_rasterio(get_vsi_url(item.format(folder), folder, None, None))

        # remove the band dimension
        da = da.squeeze().drop(labels='band')

        # add the variable
        da = da.to_dataset(name='tropospheric_NO2_column_number_density')

        if folder == 'europe':
            dates.append(datetime.datetime.strptime(item.format(folder)[-17:][:6], '%Y%m').strftime('%Y-%m-%d'))
        else: 
            dates.append(datetime.datetime.strptime(item.format(folder)[-16:][:6], '%Y%m').strftime('%Y-%m-%d'))
        
        datasets.append(da)


    ds = xr.concat(datasets, dim=pd.DatetimeIndex(dates, name='date'))


    app = ds.rest.app

    from starlette.middleware.cors import CORSMiddleware

    app.add_middleware(
                CORSMiddleware,
                allow_origins=['*'],
                allow_credentials=True,
                allow_methods=['*'],
                allow_headers=['*'],
            )

    ds.rest._app = app


    logging.info(ds)

    ds.rest.serve(port=port)

if __name__ == "__main__":
    main()