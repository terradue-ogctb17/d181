## DAPA example notebooks

We provide a few DAPA example notebooks under the [examples](http://0.0.0.0:8890/lab/tree/examples) folder.

Open the notebooks listed below and run the cells to get familiar with the DAPA API.

### Collections

[/collections/](http://0.0.0.0:8890/lab/tree/examples/01%20-%20collections.ipynb)

[/collections/{collection_id}/variables](http://0.0.0.0:8890/lab/tree/examples/02%20-%20variables.ipynb)

### Processes

[/collections/{collection_id}/processes/area:retrieve](http://0.0.0.0:8890/lab/tree/examples/03%20-%20area-retrieve.ipynb)

[/collections/{collection_id}/processes/area:aggregate-space](http://0.0.0.0:8890/lab/tree/examples/04%20-%20area-aggregate-space.ipynb)

[/collections/{collection_id}/processes/area:aggregate-time-space](http://0.0.0.0:8890/lab/tree/examples/05%20-%20area-aggregate-time-space.ipynb)

[/collections/{collection_id}/processes/area:aggregate-time](http://0.0.0.0:8890/lab/tree/examples/06%20-%20area-aggregate-time.ipynb)