
from shapely.geometry import mapping, box, shape
from shapely.wkt import loads
from shapely.geos import WKTReadingError 
from shapely.geometry import Point

def parse_location(geolocator, location):

    try: 

        return loads(location)

    except WKTReadingError:
        
        if len(location.split(',')) == 2:
            
            try: 
                coords = [float(c) for c in location.split(',')]
            except ValueError:
                
                gc = geolocator.geocode(location, geometry='geojson')
                
                if gc is not None:
                    
                    return shape(gc.raw['geojson'])
            
                else: 
                    
                    return None
                    
            # it's a lat, lon
            return Point([float(c) for c in location.split(',')][1],
                         [float(c) for c in location.split(',')][0])
        
        elif len(location.split(',')) == 4:
            
            try: 
                coords = [float(c) for c in location.split(',')]

            except ValueError:
                   
                return None
                
            # it's a bbox
            return box(*[float(c) for c in location.split(',')])
        
        elif geolocator.geocode(location, geometry='geojson') is not None:
            
            gc = geolocator.geocode(location, geometry='geojson')
            
            return shape(gc.raw['geojson'])