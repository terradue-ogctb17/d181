import uvicorn
from typing import Optional
from pydantic import BaseModel
from enum import Enum
from fastapi import FastAPI, Request
from shapely.geometry import mapping, box 
from typing import Tuple
from geopy.geocoders import Nominatim
import click
from .location import parse_location
from .cube import Cube


gdataset = None

geolocator = Nominatim(user_agent='dapa-demo')

class Collection(BaseModel):
    name: str
    description: str
    endpoint: str
        


s5p_no2 = Collection(name='S5P-NO2', description='Sentinel-5P Tropospheric NO2', endpoint='http://0.0.0.0:9012')

dapa_collections = dict()

dapa_collections['S5P-NO2'] = s5p_no2


tags_metadata = [
    { 
        "name": "Collections",
        "description": "<a href='../8001/collections/'>Documentation</a>"
    },
    {
        "name": "Processes",
        "description": "<a href='../8001/processes/'>Documentation</a>"
    },
    {
        "name": "Capabilities",
        "description": "<a href='../8001/capabilities/'>Documentation</a>"
    },
    {
        "name": "Geo",
        "description": "<a href='../8001/geo/'>Documentation</a>"
    }  
]


app = FastAPI(title="Terradue OGC Testbed 16 - DAPA",
              description="Data Access and Processing API (DAPA) for Geospatial Data",
              version="0.0.1",
              openapi_tags=tags_metadata)


class AreaFunctions(str, Enum):
    f_min = "min"
    f_max = "max"
    f_mean = "mean"
    f_stddev = "std_dev"

class Variables(str, Enum):
    tropospheric_NO2_column_number_density = 'tropospheric_NO2_column_number_density'


class Collections(str, Enum):
        
    s5p_no2 = 'S5P-NO2'
    
    
@app.get("/api", tags=["Capabilities"])
def api():
  
    return {}

@app.get("/conformance", tags=["Capabilities"])
def api():

    return {}

@app.get("/schema", tags=["Capabilities"])
def api():

    return {}


@app.get("/collections", tags=['Collections'])
async def collections():

    if gdataset == 'europe':
    
        return {'collections': [{'id': 'S5P-NO2',
                                 'title': 'Sentinel-5P NO2 column number density over Europe',
                                 'extent': {
                                     "spatial": {
                                         "bbox": [
                                             [-13, 57, 33, 33]
                                         ],
                                         "crs": "EPSG:4326"
                                     },
                                     "temporal": {
                                         "interval": [
                                             ["2018-05-01T00:00:00/2020-04-01T00:00:00"]
                                         ]
                                     }
                                 }
                                }]
               }
 
    elif gdataset == 'china':
        
        return {'collections': [{'id': 'S5P-NO2',
                                 'title': 'Sentinel-5P NO2 column number density over eastern China',
                                 'extent': {
                                     "spatial": {
                                         "bbox": [
                                             [104, 28, 123, 37]
                                         ],
                                         "crs": "EPSG:4326"
                                     },
                                     "temporal": {
                                         "interval": [
                                             ["2018-05-01T00:00:00/2020-04-01T00:00:00"]
                                         ]
                                     }
                                 }
                                }]
               }
        
    else:
        
        return None
        
        
@app.get("/collections/{collection_id}/variables", tags=["Collections"])
async def variables(collection_id: Collections):
    
    return {'variables': [{'id': 'tropospheric_NO2_column_number_density',
                           'title': 'Troposheric NO2 column number density',
                           'uom': 'umol/m2'}]
           }
    

@app.get("/collections/{collection_id}/processes/position:retrieve", tags=["Processes"])
async def position_retrieve(collection_id: Collections, location: str, variable: Variables, datetime: str='2018-05-01T00:00:00/2020-04-01T00:00:00'):
    
    aoi = parse_location(geolocator, location)
    
    if aoi is None:
 
        return {}
    
    if aoi.geometryType() == 'Point':
        
        endpoint = get_collection_endpoint()

        dapa = Cube(endpoint)
        dapa.connect()
        cube = dapa.cube

        ds = cube[variable].sel(x=aoi.x, 
                                y=aoi.y, 
                                method='nearest').sel(date=slice(datetime.split('/')[0], datetime.split('/')[1]))

        return ds.to_dict()
    
    return {}

    
@app.get("/collections/{collection_id}/processes/position:aggregate-time", tags=["Processes"])
async def position_aggregate_time(collection_id: Collections, location: str, function: AreaFunctions, variable: Variables, datetime: str='2018-05-01T00:00:00/2020-04-01T00:00:00'):

    
    aoi = parse_location(geolocator, location)
    
    if aoi is None:
 
        return {}
    
    if aoi.geometryType() == 'Point':

        endpoint = get_collection_endpoint()

        dapa = Cube(endpoint)
        dapa.connect()
        cube = dapa.cube

        ds = cube[variable].sel(x=aoi.x, 
                                y=aoi.y, 
                                method='nearest').sel(date=slice(datetime.split('/')[0], datetime.split('/')[1]))

        if function == AreaFunctions.f_mean:

            return {'mean': float(ds.mean(dim=['date', 'x', 'y']).values)}

        if function == AreaFunctions.f_min:

            return {'min': float(ds.min(dim=['date', 'x', 'y']).values)}

        if function == AreaFunctions.f_max:

            return {'max': float(ds.max(dim=['date', 'x', 'y']).values)}

        if function == AreaFunctions.f_stddev:

            return {'std-dev': float(ds.std(dim=['date', 'x', 'y']).values)}
        
        return {}
    
    else:

        return {}
    
@app.get("/collections/{collection_id}/processes/area:retrieve", tags=["Processes"])
async def area_retrieve(collection_id: Collections, location: str, variable: Variables, datetime: str='2018-05-01T00:00:00/2020-04-01T00:00:00'):
    
    aoi = parse_location(geolocator, location)
    
    if aoi is None:
 
        return None
    
    if aoi.geometryType() == 'Point':
        
        endpoint = get_collection_endpoint()
    
        dapa = Cube(endpoint)
        dapa.connect()
        cube = dapa.cube

        ds = cube[variable].sel(x=aoi.x, 
                                y=aoi.y, 
                                method='nearest').sel(date=slice(datetime.split('/')[0], datetime.split('/')[1]))
    
        return ds.to_dict()
    
    else: 
        
        if gdataset == 'europe':
                                        
            min_lon, min_lat, max_lon, max_lat = box(*[-13, 57, 33, 33]).intersection(aoi).bounds
    
        elif gdataset == 'china':
            
            min_lon, min_lat, max_lon, max_lat = box(*[104, 28, 123, 37]).intersection(aoi).bounds
        else:
            raise ValueError
    
        endpoint = get_collection_endpoint()

        dapa = Cube(endpoint)
        dapa.connect()
        cube = dapa.cube

        ds = cube[variable].sel(date=slice(datetime.split('/')[0], datetime.split('/')[1]),
                                x=slice(min_lon, max_lon), 
                                y=slice(max_lat, min_lat))

    return ds.to_dict()
    
@app.get("/collections/{collection_id}/processes/area:aggregate-space", tags=["Processes"])
async def area_aggregate_space(collection_id: Collections, location: str, function: AreaFunctions, variable: Variables, datetime: str='2018-05-01T00:00:00/2020-04-01T00:00:00'):
    
    aoi = parse_location(geolocator, location)
    
    if aoi is None:
 
        return None
    
    if aoi.geometryType() == 'Point':
        
        endpoint = get_collection_endpoint()
    
        dapa = Cube(endpoint)
        dapa.connect()
        cube = dapa.cube

        ds = cube[variable].sel(x=aoi.x, 
                                y=aoi.y, 
                                method='nearest').sel(date=slice(datetime.split('/')[0], datetime.split('/')[1]))
    
        return ds.to_dict()
    
    else: 
        
        if gdataset == 'europe':
                                             
            min_lon, min_lat, max_lon, max_lat = box(*[-13, 57, 33, 33]).intersection(aoi).bounds
    
        elif gdataset == 'china':
            
            min_lon, min_lat, max_lon, max_lat = box(*[104, 28, 123, 37]).intersection(aoi).bounds
        else:
            raise ValueError
    
        endpoint = get_collection_endpoint()

        dapa = Cube(endpoint)
        dapa.connect()
        cube = dapa.cube

        ds = cube[variable].sel(date=slice(datetime.split('/')[0], datetime.split('/')[1]),
                                x=slice(min_lon, max_lon), 
                                y=slice(max_lat, min_lat))
    
    if function == AreaFunctions.f_mean:
        
        return ds.mean(dim=['x', 'y']).to_dict()
    
    if function == AreaFunctions.f_min:
        
        return ds.min(dim=['x', 'y']).to_dict()
    
    if function == AreaFunctions.f_max:
        
        return ds.max(dim=['x', 'y']).to_dict()
    
    if function == AreaFunctions.f_stddev:
        
        return ds.std(dim=['x', 'y']).to_dict()

@app.get("/collections/{collection_id}/processes/area:aggregate-time-space", tags=["Processes"])
async def area_aggregate_time_space(collection_id: Collections, location: str, function: AreaFunctions, variable: Variables, datetime: str='2018-05-01T00:00:00/2020-04-01T00:00:00'):
    
    aoi = parse_location(geolocator, location)
    
    if aoi is None:
 
        return None
    
    if aoi.geometryType() == 'Point':
        
        endpoint = get_collection_endpoint()
    
        dapa = Cube(endpoint)
        dapa.connect()
        cube = dapa.cube

        ds = cube[variable].sel(x=aoi.x, 
                                y=aoi.y, 
                                method='nearest').sel(date=slice(datetime.split('/')[0], datetime.split('/')[1]))
    
        return ds.to_dict()
    
    else: 
        
        if gdataset == 'europe':
        
            min_lon, min_lat, max_lon, max_lat = box(*[-13, 57, 33, 33]).intersection(aoi).bounds
    
        elif gdataset == 'china':
            
            min_lon, min_lat, max_lon, max_lat = box(*[104, 28, 123, 37]).intersection(aoi).bounds
        else:
            raise ValueError
    
        endpoint = get_collection_endpoint()

        dapa = Cube(endpoint)
        dapa.connect()
        cube = dapa.cube

        ds = cube[variable].sel(date=slice(datetime.split('/')[0], datetime.split('/')[1]),
                                x=slice(min_lon, max_lon), 
                                y=slice(max_lat, min_lat))

    if function == AreaFunctions.f_mean:
        
        return {'mean': float(ds.mean(dim=['date', 'x', 'y']).values)}
    
    if function == AreaFunctions.f_min:
        
        return {'min': float(ds.min(dim=['date', 'x', 'y']).values)}
    
    if function == AreaFunctions.f_max:
        
        return {'max': float(ds.max(dim=['date', 'x', 'y']).values)}
    
    if function == AreaFunctions.f_stddev:
        
        return {'std-dev': float(ds.std(dim=['date', 'x', 'y']).values)}
    
    return {}
 
@app.get("/collections/{collection_id}/processes/area:aggregate-time", tags=["Processes"])
async def area_aggregate_time(collection_id: Collections, location: str, function: AreaFunctions, variable: Variables, datetime: str='2018-05-01T00:00:00/2020-04-01T00:00:00'):
    
    aoi = parse_location(geolocator, location)
    
    if aoi is None:
 
        return None
    
    if aoi.geometryType() == 'Point':
        
        endpoint = get_collection_endpoint()
    
        dapa = Cube(endpoint)
        dapa.connect()
        cube = dapa.cube

        ds = cube[variable].sel(x=aoi.x, 
                                y=aoi.y, 
                                method='nearest').sel(date=slice(datetime.split('/')[0], datetime.split('/')[1]))
    
        return ds.to_dict()
    
    else: 
        
        if gdataset == 'europe':
        
            min_lon, min_lat, max_lon, max_lat = box(*[-13, 57, 33, 33]).intersection(aoi).bounds
    
        elif gdataset == 'china':
            
            min_lon, min_lat, max_lon, max_lat = box(*[104, 28, 123, 37]).intersection(aoi).bounds
        else:
            raise ValueError
    
        endpoint = get_collection_endpoint()

        dapa = Cube(endpoint)
        dapa.connect()
        cube = dapa.cube

        ds = cube[variable].sel(date=slice(datetime.split('/')[0], datetime.split('/')[1]),
                                x=slice(min_lon, max_lon), 
                                y=slice(max_lat, min_lat))

    
    if function == AreaFunctions.f_mean:
        
        return ds.mean(dim=['date']).to_dict()
    
    if function == AreaFunctions.f_min:
        
        return ds.min(dim=['date']).to_dict()
    
    if function == AreaFunctions.f_max:
        
        return ds.max(dim=['date']).to_dict()
    
    if function == AreaFunctions.f_stddev:
        
        return ds.std(dim=['date']).to_dict()
 
@app.get("/geothesaurus", tags=["Geo"])
async def area_aggregate_time(location: str):
    
    aoi = parse_location(geolocator, location)
    
    if aoi is None:
 
        return None

    else:
        
        return [{'type': 'Feature', 
                 'properties': {},
                 'geometry': mapping(aoi)}]
    

def get_collection_endpoint():
    
    if gdataset == 'europe':
        return 'http://xserve-europe:9012'
    elif gdataset == 'china':
        return 'http://xserve-china:9013'
    else:
        return None
    

@click.command()
@click.option(
    "--host",
    type=str,
    default="0.0.0.0",
    help="Bind socket to this host.",
    show_default=True,
)
@click.option(
    "--port",
    type=int,
    default=8001,
    help="Bind socket to this port.",
    show_default=True,
)
@click.option(
    "--root-path",
    type=str,
    default="",
    help="Set the ASGI 'root_path' for applications submounted below a given URL path.",
)
@click.option(
    "--dataset",
    type=str,
    default="",
    help="Set the collection.",
)
def main(host, port, root_path, dataset):

    global gdataset
    gdataset = dataset
    
    uvicorn.run(app, host=host, port=port, root_path=root_path)
    
if __name__ == "__main__":
    main()

