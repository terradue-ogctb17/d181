from fsspec.implementations.http import HTTPFileSystem
import xarray as xr
import zarr

class Cube(object):
    
    def __init__(self, end_point):
        
        self.end_point = end_point
        self.cube = None 
        
        self.zarr_group = None
        
        self.dims = None
        self.data_vars = None
        self.coords = None
        
    def connect(self):
        
        fs = HTTPFileSystem()

        try:
            http_map = fs.get_mapper(self.end_point)

            self.cube = xr.open_zarr(http_map, 
                                     consolidated=True)

        except:
            
            raise
       
        self.dims = list(self.cube.dims)
        
        self.vars = list(self.cube.data_vars)
        
        self.coords = list(self.cube.coords)
        
        self.zarr_group = zarr.open_consolidated(http_map, mode='r')