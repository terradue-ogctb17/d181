## OGC Testbed 17 - D181 

### Starting the testbed services

Clone this repo and use `docker-compose` to build the docker images:

```console
docker-compose build
```

Start the service with:

```console
docker-compose up -d
```

Access the landing page at the address http://0.0.0.0:8891/

### Stopping the testbed services

Stop the service with:

```console
docker-compose down
```

### Testbed endpoints:

- Landing page http://0.0.0.0:8891/
- JupyterLab http://0.0.0.0:8890/
- DAPA API over Europe http://0.0.0.0:8000/docs
- xserver Zarr API over Europe http://0.0.0.0:9012/docs
- Dask console http://0.0.0.0:8787/status